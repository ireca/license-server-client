<?php

return array(
    'errorHandler' => array(
        'server' => isset($_ENV['SLACK_NOTIFIER_SERVER']) ? $_ENV['SLACK_NOTIFIER_SERVER'] : '',
        'transport' => array(
            'type' => 'slack',
            'params' => array(
                'hookUrl' => isset($_ENV['SLACK_NOTIFIER_HOOK_URL']) ? $_ENV['SLACK_NOTIFIER_HOOK_URL'] : '',
                'botName' => 'serverBot',
                'chanel' => isset($_ENV['SLACK_NOTIFIER_CHANEL']) ? $_ENV['SLACK_NOTIFIER_CHANEL'] : '',
            )
        ),
        'viewer' => array(
            'type' => 'browser',
            'params' => array(
                'url' => '',
                'urlParams' => array(),
            ),
        ),
    ),
    'ignoreErrors' => array(
        'stat()'
    ),
);
