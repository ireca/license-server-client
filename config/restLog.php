<?php

return array(
    'restLog' => array(
        'isActive' => true,
        'maxCodeCount' => 10,
        'databaseConfig' => array(
            'host' => isset($_ENV['DB_HOST']) ? $_ENV['DB_HOST'] : 'localhost',
            'port' => isset($_ENV['DB_PORT']) ? intval($_ENV['DB_PORT']) : 3306,
            'driver' => 'pdo_mysql',
            'user' => isset($_ENV['DB_USER']) ? $_ENV['DB_USER'] : '',
            'password' => isset($_ENV['DB_PASSWORD']) ? $_ENV['DB_PASSWORD'] : '',
            'dbname' => isset($_ENV['DB_NAME']) ? $_ENV['DB_NAME'] : '',
            'charset' => 'UTF8',
        )
    )
);
