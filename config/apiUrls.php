<?php

return array(
    'users' => array(
        'user' => 'api/v1/user',
        'users' => 'api/v1/users',
        'create' => 'api/v1/users',
        'update' => 'api/v1/users',
        'delete' => 'api/v1/users',
    ),
    'licenses' => array(
        'license' => 'api/v1/license',
        'create' => 'api/v1/licenses/create',
        'replace' => 'api/v1/licenses/replace',
        'delete' => 'api/v1/licenses',
    ),
);
