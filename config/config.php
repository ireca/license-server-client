<?php

use LicenseServerClient\Types\ProtocolType;
use Symfony\Component\Dotenv\Dotenv;

(new Dotenv())->load(dirname(__DIR__).'/.env');

return array(
    'licenseServerClient' => array(
        'requestClient' => ProtocolType::HTTP_REQUEST_SERVICE,
        'apiUrls' => require('apiUrls.php'),
        'httpClientOptions' => require('httpClientOptions.php'),
        'requestConfig' => require('requestConfig.php'),
        'restLogConfig' => require('restLog.php'),
        'slackNotifier' => require('slackNotifier.php'),
    )
);
