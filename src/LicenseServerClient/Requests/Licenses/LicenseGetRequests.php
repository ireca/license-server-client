<?php
namespace LicenseServerClient\Requests\Licenses;

use LicenseServerClient\Requests\AbstractBaseRequest;
use LicenseServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\Collection;

class LicenseGetRequests extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $customerId = '';

    public $productType = '';

    /**
     * @param string $customerId
     * @param $productType
     */
    public function __construct($customerId, $productType = '')
    {
        $this->customerId = $customerId;
        $this->productType = $productType;
    }

    /**
     * @return Collection
     */
    public function getConstraints()
    {
        return new Collection(array(
            'customerId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'productType' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            )
        ));
    }
}
