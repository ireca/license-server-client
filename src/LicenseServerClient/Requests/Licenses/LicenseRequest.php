<?php
namespace LicenseServerClient\Requests\Licenses;

use LicenseServerClient\Requests\AbstractBaseRequest;
use LicenseServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class LicenseRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $licenseId = '';

    /**
     * @var string
     */
    public $productType = '';

    /**
     * @var string
     */
    public $callbackUrl = '';

    /**
     * @var int
     */
    public $count;

    /**
     * @var string
     */
    public $licenseKey = '';

    /**
     * @var string 
     */
    public $data = '';

    /**
     * @var int
     */
    public $duration = 0;

    /**
     * @var string
     */
    public $activationAt = '';

    /**
     * @var string
     */
    public $expirationAt = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @param string $licenseId
     * @param string $productType
     * @param string $callbackUrl
     * @param int $count
     * @param string $licenseKey
     * @param string $activationAt
     * @param string $expirationAt
     * @param string $description
     */
    public function __construct($licenseId, $productType, $callbackUrl, $count, $licenseKey, $activationAt, $expirationAt, $data = '', $duration = 0, $description = '')
    {
        $this->licenseId = $licenseId;
        $this->productType = $productType;
        $this->callbackUrl = $callbackUrl;
        $this->count = $count;
        $this->licenseKey = $licenseKey;
        $this->activationAt = $activationAt;
        $this->expirationAt = $expirationAt;
        $this->data = $data;
        $this->duration = $duration;
        $this->description = $description;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'licenseId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'productType' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'callbackUrl' => array(
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
                new Constraints\Url(),
            ),
            'count' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('integer'),
            ),
            'licenseKey' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'data' => array(
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'duration' => array(
                new Constraints\PositiveOrZero(),
                new Constraints\Type('integer'),
            ),
            'activationAt' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
                new Constraints\DateTime(),
            ),
            'expirationAt' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
                new Constraints\DateTime(),
            ),
            'description' => array(
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
        ));
    }
}
