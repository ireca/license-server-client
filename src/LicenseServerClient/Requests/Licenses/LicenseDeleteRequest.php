<?php
namespace LicenseServerClient\Requests\Licenses;

use LicenseServerClient\Requests\AbstractBaseRequest;
use LicenseServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\Collection;

class LicenseDeleteRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $customerId = '';

    /**
     * @var string
     */
    public $licenseId = '';

    /**
     * @param string $customerId
     * @param string $licenseId
     */
    public function __construct($customerId, $licenseId)
    {
        $this->customerId = $customerId;
        $this->licenseId = $licenseId;
    }

    /**
     * @return Collection
     */
    public function getConstraints()
    {
        return new Collection(array(
            'customerId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'licenseId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            )
        ));
    }
}
