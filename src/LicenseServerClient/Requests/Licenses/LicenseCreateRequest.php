<?php
namespace LicenseServerClient\Requests\Licenses;

use LicenseServerClient\Requests\AbstractBaseRequest;
use LicenseServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class LicenseCreateRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $customerId = '';

    /**
     * @var string
     */
    public $type = '';

    /**
     * @var string
     */
    public $inn = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $licenseKey = '';

    /**
     * @var string
     */
    public $data = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var LicenseRequest[]
     */
    public $licenses = array();

    /**
     * @param $customerId
     * @param $type
     * @param $inn
     * @param $title
     * @param $customerLicenseKey
     * @param $data
     * @param $description
     * @param array $licenses
     */
    public function __construct($customerId, $type, $inn, $title, array $licenses, $customerLicenseKey = '', $data = '', $description = '')
    {
        $this->customerId = $customerId;
        $this->type = $type;
        $this->inn = $inn;
        $this->title = $title;
        $this->licenseKey = $customerLicenseKey;
        $this->data = $data;
        $this->description = $description;
        $this->licenses = $licenses;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'customerId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'type' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'inn' => array(
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'title' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'licenseKey' => array(
                new Constraints\Type('string'),
            ),
            'data' => array(
                new Constraints\Type('string'),
            ),
            'description' => array(
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'licenses' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('array'),
                new Constraints\All(
                    new Constraints\Collection(array(
                        'licenseId' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                        ),
                        'productType' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                        ),
                        'callbackUrl' => array(
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                            new Constraints\Url(),
                        ),
                        'count' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('integer'),
                        ),
                        'licenseKey' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                        ),
                        'activationAt' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                            new Constraints\DateTime(),
                        ),
                        'expirationAt' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                            new Constraints\DateTime(),
                        ),
                        'data' => array(
                            new Constraints\Type('string'),
                        ),
                        'duration' => array(
                            new Constraints\PositiveOrZero(),
                            new Constraints\Type('integer'),
                        ),
                        'description' => array(
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                        ),
                    ))
                )
            )
        ));
    }
}
