<?php
namespace LicenseServerClient\Requests;

interface RequestInterface
{
    /**
     * @return array
     */
    public function getConstraints();

    /**
     * @return bool
     */
    public function validate();

    /**
     * @return array
     */
    public function toArray();
}
