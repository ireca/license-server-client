<?php
namespace LicenseServerClient\Requests\Users;

use LicenseServerClient\Requests\AbstractBaseRequest;
use LicenseServerClient\Requests\RequestInterface;
use LicenseServerClient\Types\PermissionType;
use Symfony\Component\Validator\Constraints;

class ProductRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $type = '';

    /**
     * @var array
     */
    public $permissions = array();

    /**
     * @param string $type
     * @param array $permissions
     */
    public function __construct($type, array $permissions)
    {
        $this->type = $type;
        $this->permissions = $permissions;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'type' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'permissions' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('array'),
                new Constraints\Choice(array(
                    'choices' => array(PermissionType::CREATE, PermissionType::UPDATE, PermissionType::DELETE, PermissionType::GET),
                    'multiple' => true))
            )
        ));
    }
}
