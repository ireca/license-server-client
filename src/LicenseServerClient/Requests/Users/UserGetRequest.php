<?php
namespace LicenseServerClient\Requests\Users;

use LicenseServerClient\Requests\AbstractBaseRequest;
use LicenseServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\Collection;

class UserGetRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $userId = '';

    /**
     * @param string $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return Collection
     */
    public function getConstraints()
    {
        return new Collection(array(
            'userId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            )
        ));
    }
}
