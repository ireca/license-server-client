<?php
namespace LicenseServerClient\Requests\Users;

use LicenseServerClient\Requests\AbstractBaseRequest;
use LicenseServerClient\Requests\RequestInterface;
use LicenseServerClient\Types\PermissionType;
use Symfony\Component\Validator\Constraints;

class UserCreateRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $userId = '';

    /**
     * @var string
     */
    public $role = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var ProductRequest[]
     */
    public $products = array();

    /**
     * @param string $userId
     * @param string $role
     * @param string $title
     * @param string $description
     * @param array $products
     */
    public function __construct($userId, $role, $title, $description, array $products)
    {
        $this->userId = $userId;
        $this->role = $role;
        $this->title = $title;
        $this->description = $description;
        $this->products = $products;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'userId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'role' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
                new Constraints\Choice(array('choices' => array('admin', 'service'))),
            ),
            'title' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'description' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 255)),
            ),
            'products' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('array'),
                new Constraints\All(
                    new Constraints\Collection(array(
                        'type' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 255)),
                        ),
                        'permissions' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('array'),
                            new Constraints\Choice(array(
                                'choices' => array(PermissionType::CREATE, PermissionType::UPDATE, PermissionType::DELETE, PermissionType::GET),
                                'multiple' => true))
                        )
                    ))
                )
            ),
        ));
    }
}
