<?php
namespace LicenseServerClient\Types;

class PermissionType
{
    const CREATE = 'create';

    const UPDATE = 'update';

    const DELETE = 'delete';

    const GET = 'get';
}
