<?php
namespace LicenseServerClient\Responses;

abstract class AbstractBaseResponse
{
    /**
     * @param array $attributes
     *
     * @return $this
     */
    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $attribute => $value) {
            $this->setPropertyValue($attribute, $value);
        }

        return $this;
    }

    /**
     * @param string $property
     * @param string $value
     *
     * @return void
     */
    private function setPropertyValue($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->{$property} = $value;
        }
    }
}
