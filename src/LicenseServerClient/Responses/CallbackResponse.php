<?php
namespace LicenseServerClient\Responses;

class CallbackResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $customerId = '';

    /**
     * @var string
     */
    public $licenseId = '';

    /**
     * @var string
     */
    public $productType = '';

    /**
     * @var string
     */
    public $callbackUrl = '';

    /**
     * @var int
     */
    public $count = 0;

    /**
     * @var string
     */
    public $licenseKey = '';

    /**
     * @var string
     */
    public $activationAt = '';

    /**
     * @var string
     */
    public $expirationAt = '';

    /**
     * @var string
     */
    public $description = '';
}
