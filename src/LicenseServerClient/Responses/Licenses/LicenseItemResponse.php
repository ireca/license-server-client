<?php
namespace LicenseServerClient\Responses\Licenses;

use LicenseServerClient\Responses\AbstractBaseResponse;

class LicenseItemResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $customerId = '';

    /**
     * @var string
     */
    public $type = '';

    /**
     * @var string
     */
    public $inn = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $licenseKey = '';

    /**
     * @var string
     */
    public $data = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var LicenseResponse[]
     */
    public $licenses = array();
}
