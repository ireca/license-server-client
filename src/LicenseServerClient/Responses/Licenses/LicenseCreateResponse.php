<?php
namespace LicenseServerClient\Responses\Licenses;

use LicenseServerClient\Responses\AbstractBaseResponse;

class LicenseCreateResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $licenseId = '';

    /**
     * @var string
     */
    public $licenseKey = '';
}
