<?php
namespace LicenseServerClient\Responses\Licenses;

use Doctrine\Common\Collections\ArrayCollection;

class LicensesReplaceResponseList extends ArrayCollection
{
}
