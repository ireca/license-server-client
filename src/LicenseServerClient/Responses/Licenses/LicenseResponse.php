<?php
namespace LicenseServerClient\Responses\Licenses;

use LicenseServerClient\Responses\AbstractBaseResponse;

class LicenseResponse extends AbstractBaseResponse
{
    public $licenseId = '';

    public $productType = '';

    public $callbackUrl = '';

    public $count = 0;

    public $licenseKey = '';

    public $activationAt = '';

    public $expirationAt = '';

    public $data = '';

    public $duration = 0;

    public $description = '';
}