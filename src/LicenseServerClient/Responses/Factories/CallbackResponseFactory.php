<?php
namespace LicenseServerClient\Responses\Factories;

use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Responses\CallbackResponse;

class CallbackResponseFactory
{
    /**
     * @param string $callback
     *
     * @return CallbackResponse
     *
     * @throws LicenseClientException
     */
    public static function create($callback)
    {
        $attributes = json_decode($callback, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        $callback = new CallbackResponse();
        $callback->setAttributes($attributes);

        return $callback;
    }
}
