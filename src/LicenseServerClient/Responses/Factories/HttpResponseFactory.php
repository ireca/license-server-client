<?php
namespace LicenseServerClient\Responses\Factories;

use GuzzleHttp\Psr7\Response;
use LicenseServerClient\Responses\LicenseServerResponse;

class HttpResponseFactory
{
    /**
     * @param Response $response
     *
     * @return LicenseServerResponse
     */
    public static function create(Response $response)
    {
        $body = $response->getBody();

        return new LicenseServerResponse($response->getStatusCode(), $body->getContents());
    }
}
