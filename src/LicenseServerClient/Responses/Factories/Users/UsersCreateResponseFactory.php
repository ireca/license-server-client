<?php
namespace LicenseServerClient\Responses\Factories\Users;

use LicenseServerClient\Responses\Users\UsersCreateResponse;

class UsersCreateResponseFactory
{
    /**
     * @param array $response
     *
     * @return UsersCreateResponse
     */
    public static function create(array $response)
    {
        $userCreate = new UsersCreateResponse();
        $userCreate->setAttributes($response);

        return $userCreate;
    }
}
