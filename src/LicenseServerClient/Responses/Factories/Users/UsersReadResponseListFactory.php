<?php
namespace LicenseServerClient\Responses\Factories\Users;

use LicenseServerClient\Responses\Users\UserReadResponse;
use LicenseServerClient\Responses\Users\UsersReadResponseList;

class UsersReadResponseListFactory
{
    /**
     * @param array $response
     *
     * @return UsersReadResponseList
     */
    public static function create(array $response)
    {
        $listUsers = new UsersReadResponseList();
        foreach ($response as $attributes) {
            $user = new UserReadResponse();
            $user->setAttributes($attributes);
            $listUsers->add($user);
        }

        return $listUsers;
    }
}
