<?php
namespace LicenseServerClient\Responses\Factories\Users;

use LicenseServerClient\Responses\Users\UserReadResponse;

class UserReadResponseFactory
{
    /**
     * @param array $response
     *
     * @return UserReadResponse
     */
    public static function create(array $response)
    {
        $user = new UserReadResponse();
        $user->setAttributes($response);

        return $user;
    }
}
