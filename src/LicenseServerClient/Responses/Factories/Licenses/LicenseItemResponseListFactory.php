<?php
namespace LicenseServerClient\Responses\Factories\Licenses;

use LicenseServerClient\Responses\Licenses\LicenseItemResponse;
use LicenseServerClient\Responses\Licenses\LicenseResponse;

class LicenseItemResponseListFactory
{
    /**
     * @param $response
     *
     * @return LicenseItemResponse
     */
    public static function create($response)
    {
        $licenseResponse = new LicenseItemResponse();
        $licenseResponse->setAttributes($response);
        $licenseResponse->licenses = array();
        foreach ($response['licenses'] as $license) {
            $licenseItemResponse = new LicenseResponse();
            $licenseItemResponse->setAttributes($license);
            $licenseResponse->licenses[] = $licenseItemResponse;
        }

        return $licenseResponse;
    }
}