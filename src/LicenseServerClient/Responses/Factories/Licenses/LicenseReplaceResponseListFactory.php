<?php
namespace LicenseServerClient\Responses\Factories\Licenses;

use LicenseServerClient\Responses\Licenses\LicenseCreateResponse;
use LicenseServerClient\Responses\Licenses\LicensesReplaceResponseList;

class LicenseReplaceResponseListFactory
{
    /**
     * @param array $response
     *
     * @return LicensesReplaceResponseList
     */
    public static function create(array $response)
    {
        $listLicenses = new LicensesReplaceResponseList();
        foreach ($response as $attributes) {
            $license = new LicenseCreateResponse();
            $license->setAttributes($attributes);
            $listLicenses->add($license);
        }

        return $listLicenses;
    }
}
