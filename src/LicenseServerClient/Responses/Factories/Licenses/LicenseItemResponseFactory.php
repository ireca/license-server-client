<?php
namespace LicenseServerClient\Responses\Factories\Licenses;

use LicenseServerClient\Responses\Licenses\LicenseItemResponse;

class LicenseItemResponseFactory
{
    /**
     * @param array $response
     *
     * @return LicenseItemResponse
     */
    public static function create(array $response)
    {
        $licenseItem = new LicenseItemResponse();
        $licenseItem->setAttributes($response);

        return $licenseItem;
    }
}
