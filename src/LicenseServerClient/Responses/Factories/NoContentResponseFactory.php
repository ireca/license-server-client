<?php
namespace LicenseServerClient\Responses\Factories;

use LicenseServerClient\Responses\NoContentResponse;

class NoContentResponseFactory
{
    /**
     * @return NoContentResponse
     */
    public static function create()
    {
        return new NoContentResponse();
    }
}
