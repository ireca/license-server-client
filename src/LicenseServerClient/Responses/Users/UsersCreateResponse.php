<?php
namespace LicenseServerClient\Responses\Users;

use LicenseServerClient\Responses\AbstractBaseResponse;

class UsersCreateResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $token = '';
}
