<?php
namespace LicenseServerClient\Responses\Users;

use LicenseServerClient\Responses\AbstractBaseResponse;

class UserReadResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $userId = '';

    /**
     * @var string
     */
    public $role = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var array
     */
    public $products = array();
}
