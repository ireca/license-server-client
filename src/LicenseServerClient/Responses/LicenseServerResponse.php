<?php
namespace LicenseServerClient\Responses;

class LicenseServerResponse
{
    /**
     * @var integer
     */
    public $code = 0;

    /**
     * @var string
     */
    public $content = '';

    function __construct($code, $content)
    {
        $this->code = $code;
        $this->content = $content;
    }
}
