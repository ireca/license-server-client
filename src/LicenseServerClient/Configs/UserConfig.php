<?php
namespace LicenseServerClient\Configs;

class UserConfig extends AbstractBaseConfig
{
    /**
     * @var string
     */
    private $user = '';

    /**
     * @var string
     */
    private $users = '';

    /**
     * @var string
     */
    private $create = '';

    /**
     * @var string
     */
    private $update = '';

    /**
     * @var string
     */
    private $delete = '';

    /**
     * @param string $user
     * @param string $create
     * @param string $update
     * @param string $delete
     * @param string $users
     */
    public function __construct($user, $create, $update, $delete, $users)
    {
        $this->user = $user;
        $this->create = $create;
        $this->update = $update;
        $this->delete = $delete;
        $this->users = $users;
    }

    /**
     * @return string
     */
    public function getUserUrl()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->create;
    }

    /**
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->update;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->delete;
    }

    /**
     * @return string
     */
    public function getUsersUrl()
    {
        return $this->users;
    }
}
