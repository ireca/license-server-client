<?php
namespace LicenseServerClient\Configs;

class LicenseConfig extends AbstractBaseConfig
{
    /**
     * @var string
     */
    private $license = '';

    /**
     * @var string
     */
    private $create = '';

    /**
     * @var string
     */
    private $replace = '';

    /**
     * @var string
     */
    private $delete = '';

    /**
     * @param $license
     * @param $create
     * @param string $replace
     * @param string $delete
     */

    public function __construct($license, $create, $replace, $delete)
    {
        $this->license = $license;
        $this->create = $create;
        $this->replace = $replace;
        $this->delete = $delete;
    }

    /**
     * @return string
     */
    public function getLicenseUrl()
    {
        return $this->license;
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->create;
    }

    /**
     * @return string
     */
    public function getReplaceUrl()
    {
        return $this->replace;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->delete;
    }
}
