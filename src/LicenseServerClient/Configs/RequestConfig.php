<?php
namespace LicenseServerClient\Configs;

class RequestConfig extends AbstractBaseConfig
{
    /**
     * @var int
     */
    private $retryConnect = 3;

    /**
     * @var array
     */
    private $errorConnectHttpCodes = array();

    /**
     * @param int $retryConnect
     * @param array $errorConnectHttpCodes
     */
    public function __construct($retryConnect, array $errorConnectHttpCodes)
    {
        $this->retryConnect = $retryConnect;
        $this->errorConnectHttpCodes = $errorConnectHttpCodes;
    }

    /**
     * @return int
     */
    public function getRetryConnect()
    {
        return $this->retryConnect;
    }

    /**
     * @return array
     */
    public function getErrorConnectHttpCodes()
    {
        return $this->errorConnectHttpCodes;
    }
}
