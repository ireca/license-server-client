<?php
namespace LicenseServerClient\Services\Factories;

use GuzzleHttp\Client;
use LicenseServerClient\Services\ConfigService;
use LicenseServerClient\Services\HttpRequestService;
use LicenseServerClient\Services\RequestServiceInterface;

class HttpRequestServiceFactory
{
    /**
     * @param ConfigService $configService
     *
     * @return RequestServiceInterface
     */
    public static function create(ConfigService $configService)
    {
        $config = array(
            'base_uri' => $configService->getHttpClientOptions()['baseUri'],
            'timeout' => $configService->getHttpClientOptions()['timeout'],
            'headers' => $configService->getHttpClientOptions()['headers']
        );

        return new HttpRequestService(
            new Client($config),
            $configService->getRequestServiceOptions()
        );
    }
}
