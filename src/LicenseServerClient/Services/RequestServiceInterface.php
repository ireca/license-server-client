<?php
namespace LicenseServerClient\Services;

use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Responses\LicenseServerResponse;

interface RequestServiceInterface
{
    /**
     * @param string $url
     * @param string $method
     * @param array $request
     *
     * @return LicenseServerResponse
     *
     * @throws LicenseClientException
     */
    public function send($url, $method, array $request = array());
}
