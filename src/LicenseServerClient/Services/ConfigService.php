<?php
namespace LicenseServerClient\Services;

use Exception;
use LicenseServerClient\Components\Configuration;
use LicenseServerClient\Configs\LicenseConfig;
use LicenseServerClient\Configs\RequestConfig;
use LicenseServerClient\Configs\UserConfig;
use Symfony\Component\Config\Definition\Processor;

class ConfigService
{

    /**
     * @var array
     */
    private $config;

    /**
     * @var Configuration|null
     */
    private $configuration;

    /**
     * @var string
     */
    private $errorMessage = '';

    /**
     * @var string
     */
    private $requestClient = '';

    /**
     * @var array
     */
    private $httpClientOptions = array();

    /**
     * @var RequestConfig
     */
    private $requestServiceOptions;

    /**
     * @var UserConfig
     */
    private $userConfig;

    /**
     * @var LicenseConfig
     */
    private $licenseConfig;

    /**
     * @var array
     */
    private $restLogConfig = array();

    /**
     * @var array
     */
    private $slackNotifier = array();

    /**
     * ConfigService constructor.
     *
     * @param Configuration $configuration
     * @param array $config
     */
    public function __construct(Configuration $configuration, $config)
    {
        $this->configuration = $configuration;
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isValidate()
    {
        $processor = new Processor();
        try {
            $this->setConfig($processor->processConfiguration($this->configuration, $this->config));
        } catch (Exception $ex) {
            $this->setErrorMessage($ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @return string
     */
    public function getRequestClient()
    {
        return $this->requestClient;
    }

    /**
     * @return array
     */
    public function getHttpClientOptions()
    {
        return $this->httpClientOptions;
    }

    /**
     * @return RequestConfig
     */
    public function getRequestServiceOptions()
    {
        return $this->requestServiceOptions;
    }

    /**
     * @return UserConfig
     */
    public function getUserConfig()
    {
        return $this->userConfig;
    }

    /**
     * @return LicenseConfig
     */
    public function getLicenseConfig()
    {
        return $this->licenseConfig;
    }

    /**
     * @return array
     */
    public function getRestLogConfig()
    {
        return $this->restLogConfig;
    }

    /**
     * @return array
     */
    public function getSlackNotifier()
    {
        return $this->slackNotifier;
    }

    /**
     * @param array $config
     */
    protected function setConfig($config)
    {
        $this->setRequestClient($config['requestClient']);
        $this->setHttpClientOptions($config['httpClientOptions']);
        $this->setRequestServiceOptions($config['requestConfig']);
        $this->setUserConfig($config['apiUrls']['users']);
        $this->setLicenseConfig($config['apiUrls']['licenses']);
        $this->setRestLogConfig($config['restLogConfig']);
        $this->setSlackNotifier($config['slackNotifier']);
    }

    /**
     * @param string $errorMessage
     */
    private function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @param string $requestClient
     */
    private function setRequestClient($requestClient)
    {
        $this->requestClient = $requestClient;
    }

    /**
     * @param array $config
     */
    private function setHttpClientOptions(array $config)
    {
        $this->httpClientOptions = $config;
    }

    /**
     * @param array $config
     */
    private function setRequestServiceOptions(array $config)
    {
        $this->requestServiceOptions = new RequestConfig($config['retryConnect'], $config['errorConnectHttpCodes']);
    }

    /**
     * @param array $config
     */
    private function setUserConfig(array $config)
    {
        $this->userConfig = new UserConfig($config['user'], $config['create'], $config['update'], $config['delete'], $config['users']);
    }

    /**
     * @param array $config
     */
    private function setLicenseConfig(array $config)
    {
        $this->licenseConfig = new LicenseConfig($config['license'], $config['create'], $config['replace'], $config['delete']);
    }

    /**
     * @param array $config
     */
    private function setRestLogConfig(array $config)
    {
        $this->restLogConfig = $config;
    }

    /**
     * @param array $config
     */
    private function setSlackNotifier(array $config)
    {
        $this->slackNotifier = $config;
    }
}
