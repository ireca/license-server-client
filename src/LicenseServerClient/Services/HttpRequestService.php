<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
namespace LicenseServerClient\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use LicenseServerClient\Configs\RequestConfig;
use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Responses\Factories\HttpResponseFactory;
use LicenseServerClient\Responses\LicenseServerResponse;

class HttpRequestService implements RequestServiceInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var RequestConfig
     */
    protected $options;

    /**
     * @param Client $client
     * @param RequestConfig $options
     */
    public function __construct(Client $client, RequestConfig $options)
    {
        $this->client = $client;
        $this->options = $options;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $request
     *
     * @return LicenseServerResponse
     *
     * @throws LicenseClientException
     */
    public function send($url, $method, array $request = array())
    {
        $errorMessage = '';
        $errorCode = 0;
        $response = new LicenseServerResponse(0, '');
        for ($i = 1; $i <= $this->getOptions()->getRetryConnect(); $i++) {
            try {
                $response = $this->getClient()->request($method, $url, $request);
                break;
            } catch (GuzzleException $ex) {
                $errorMessage = $ex->getMessage();
                $errorCode = $ex->getCode();
                $response = new LicenseServerResponse($errorCode, $errorMessage);
                if (! in_array($errorCode, $this->getOptions()->getErrorConnectHttpCodes())) {
                    break;
                }
            }
        }

        if ($errorMessage !== '' && $errorCode !== 0) {
            throw new LicenseClientException($errorMessage, $errorCode);
        }

        /** @noinspection PhpUndefinedVariableInspection */
        return HttpResponseFactory::create($response);
    }

    /**
     * @return Client
     */
    private function getClient()
    {
        return $this->client;
    }

    /**
     * @return RequestConfig
     */
    private function getOptions()
    {
        return $this->options;
    }
}
