<?php
namespace LicenseServerClient\Services\Api;

use LicenseServerClient\Requests\Users\UserCreateRequest;
use LicenseServerClient\Requests\Users\UserDeleteRequest;
use LicenseServerClient\Requests\Users\UserGetRequest;
use LicenseServerClient\Requests\Users\UsersGetRequest;
use LicenseServerClient\Responses\NoContentResponse;
use LicenseServerClient\Responses\Users\UserReadResponse;
use LicenseServerClient\Responses\Users\UsersCreateResponse;
use LicenseServerClient\Responses\Users\UsersReadResponseList;

interface UserApiServiceInterface
{
    /**
     * @param UserGetRequest $request
     *
     * @return UserReadResponse
     */
    public function getUser(UserGetRequest $request);

    /**
     * @param UsersGetRequest $request
     *
     * @return UsersReadResponseList
     */
    public function getUsers(UsersGetRequest $request);

    /**
     * @param UserCreateRequest $request
     *
     * @return UsersCreateResponse
     */
    public function create(UserCreateRequest $request);

    /**
     * @param UserCreateRequest $request
     *
     * @return NoContentResponse
     */
    public function update(UserCreateRequest $request);

    /**
     * @param UserDeleteRequest $request
     *
     * @return NoContentResponse
     */
    public function delete(UserDeleteRequest $request);
}
