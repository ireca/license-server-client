<?php
namespace LicenseServerClient\Services\Api;

use Exception;
use LicenseServerClient\Configs\LicenseConfig;
use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Requests\Licenses\LicenseCreateRequest;
use LicenseServerClient\Requests\Licenses\LicenseDeleteRequest;
use LicenseServerClient\Requests\Licenses\LicenseGetRequest;
use LicenseServerClient\Requests\Licenses\LicenseGetRequests;
use LicenseServerClient\Responses\Factories\Licenses\LicenseItemResponseFactory;
use LicenseServerClient\Responses\Factories\Licenses\LicenseItemResponseListFactory;
use LicenseServerClient\Responses\Factories\Licenses\LicenseReplaceResponseListFactory;
use LicenseServerClient\Responses\Factories\NoContentResponseFactory;
use LicenseServerClient\Responses\Licenses\LicenseItemResponse;
use LicenseServerClient\Responses\Licenses\LicenseItemResponseList;
use LicenseServerClient\Responses\Licenses\LicensesReplaceResponseList;
use LicenseServerClient\Responses\NoContentResponse;
use LicenseServerClient\Services\RequestServiceInterface;
use LicenseServerClient\Types\HttpStatusCodeType;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\ErrorHandlerService;

class LicenseApiService extends AbstractBaseLicenseServer implements LicenseApiServiceInterface
{

    /**
     * @var LicenseConfig
     */
    private $licenseConfig = null;

    /**
     * @param LicenseConfig $licenseConfig
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     */
    public function __construct(LicenseConfig $licenseConfig, RequestServiceInterface $requestService, MessageLogService $logger, ErrorHandlerService $notifier)
    {
        parent::__construct($requestService, $logger, $notifier);
        $this->licenseConfig = $licenseConfig;
    }

    /**
     * @param LicenseGetRequest $request
     *
     * @return LicenseItemResponse|null
     *
     * @throws LicenseClientException|Exception
     */
    public function getLicense(LicenseGetRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendGetRequest($request->toArray(), $this->getLicenseConfig()->getLicenseUrl());
        if (! in_array($response->code, array(HttpStatusCodeType::OK, HttpStatusCodeType::NO_CONTENT))) {
            throw new LicenseClientException($response->content, $response->code);
        }

        if ($response->code == HttpStatusCodeType::NO_CONTENT) {

            return null;
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        return LicenseItemResponseFactory::create($content);
    }

    /**
     * @param LicenseGetRequests $request
     *
     * @return LicenseItemResponse|null
     *
     * @throws LicenseClientException|Exception
     */
    public function getLicenses(LicenseGetRequests $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendGetRequest($request->toArray(), $this->getLicenseConfig()->getLicenseUrl());
        if (! in_array($response->code, array(HttpStatusCodeType::OK, HttpStatusCodeType::NO_CONTENT))) {
            throw new LicenseClientException($response->content, $response->code);
        }

        if ($response->code == HttpStatusCodeType::NO_CONTENT) {

            return null;
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        return LicenseItemResponseListFactory::create($content);
    }

    /**
     * @param LicenseCreateRequest $request
     *
     * @return LicensesReplaceResponseList
     *
     * @throws LicenseClientException|Exception
     */
    public function replace(LicenseCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendPostRequest($request->toArray(), $this->getLicenseConfig()->getReplaceUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new LicenseClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        return LicenseReplaceResponseListFactory::create($content);
    }

    /**
     * @param LicenseCreateRequest $request
     *
     * @return LicensesReplaceResponseList
     *
     * @throws LicenseClientException|Exception
     */
    public function create(LicenseCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendPostRequest($request->toArray(), $this->getLicenseConfig()->getCreateUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new LicenseClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        return LicenseReplaceResponseListFactory::create($content);
    }

    /**
     * @param LicenseDeleteRequest $request
     *
     * @return NoContentResponse
     *
     * @throws LicenseClientException|Exception
     */
    public function delete(LicenseDeleteRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendDeleteRequest($request->toArray(), $this->getLicenseConfig()->getDeleteUrl());
        if ($response->code !== HttpStatusCodeType::ACCEPTED) {
            throw new LicenseClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @return LicenseConfig
     */
    private function getLicenseConfig()
    {
        return $this->licenseConfig;
    }
}
