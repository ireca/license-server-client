<?php
namespace LicenseServerClient\Services\Api;

use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Requests\Licenses\LicenseCreateRequest;
use LicenseServerClient\Requests\Licenses\LicenseDeleteRequest;
use LicenseServerClient\Requests\Licenses\LicenseGetRequest;
use LicenseServerClient\Requests\Licenses\LicenseGetRequests;
use LicenseServerClient\Responses\Licenses\LicenseItemResponse;
use LicenseServerClient\Responses\Licenses\LicenseItemResponseList;
use LicenseServerClient\Responses\Licenses\LicensesReplaceResponseList;
use LicenseServerClient\Responses\NoContentResponse;

interface LicenseApiServiceInterface
{
    /**
     * @param LicenseGetRequest $request
     *
     * @return LicenseItemResponse
     *
     * @throws LicenseClientException|\Exception
     */
    public function getLicense(LicenseGetRequest $request);

    /**
     * @param LicenseGetRequests $request
     *
     * @return LicenseItemResponse
     *
     * @throws LicenseClientException|\Exception
     */
    public function getLicenses(LicenseGetRequests $request);

    /**
     * @param LicenseCreateRequest $request
     *
     * @return LicensesReplaceResponseList
     *
     * @throws LicenseClientException|\Exception
     */
    public function create(LicenseCreateRequest $request);

    /**
     * @param LicenseCreateRequest $request
     *
     * @return LicensesReplaceResponseList
     *
     * @throws LicenseClientException|\Exception
     */
    public function replace(LicenseCreateRequest $request);

    /**
     * @param LicenseDeleteRequest $request
     *
     * @return NoContentResponse
     *
     * @throws LicenseClientException|\Exception
     */
    public function delete(LicenseDeleteRequest $request);
}
