<?php
namespace LicenseServerClient\Services\Api;

use Exception;
use LicenseServerClient\Configs\UserConfig;
use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Requests\Users\UserCreateRequest;
use LicenseServerClient\Requests\Users\UserDeleteRequest;
use LicenseServerClient\Requests\Users\UserGetRequest;
use LicenseServerClient\Requests\Users\UsersGetRequest;
use LicenseServerClient\Responses\Factories\NoContentResponseFactory;
use LicenseServerClient\Responses\Factories\Users\UserReadResponseFactory;
use LicenseServerClient\Responses\Factories\Users\UsersCreateResponseFactory;
use LicenseServerClient\Responses\Factories\Users\UsersReadResponseListFactory;
use LicenseServerClient\Responses\NoContentResponse;
use LicenseServerClient\Responses\Users\UserReadResponse;
use LicenseServerClient\Responses\Users\UsersCreateResponse;
use LicenseServerClient\Responses\Users\UsersReadResponseList;
use LicenseServerClient\Services\RequestServiceInterface;
use LicenseServerClient\Types\HttpStatusCodeType;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\ErrorHandlerService;

class UserApiService extends AbstractBaseLicenseServer implements UserApiServiceInterface
{
    /**
     * @var UserConfig
     */
    private $userConfig = null;

    /**
     * @param UserConfig $userConfig
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     */
    public function __construct(UserConfig $userConfig, RequestServiceInterface $requestService, MessageLogService $logger, ErrorHandlerService $notifier)
    {
        parent::__construct($requestService, $logger, $notifier);
        $this->userConfig = $userConfig;
    }

    /**
     * @param UserGetRequest $request
     *
     * @return UserReadResponse
     *
     * @throws LicenseClientException|Exception
     */
    public function getUser(UserGetRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendGetRequest($request->toArray(), $this->getUserConfig()->getUserUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new LicenseClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        return UserReadResponseFactory::create($content);
    }

    /**
     * @param UsersGetRequest $request
     *
     * @return UsersReadResponseList
     *
     * @throws LicenseClientException|Exception
     */
    public function getUsers(UsersGetRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendGetRequest($request->toArray(), $this->getUserConfig()->getUsersUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new LicenseClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        return UsersReadResponseListFactory::create($content);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return UsersCreateResponse
     *
     * @throws LicenseClientException|Exception
     */
    public function create(UserCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendPostRequest($request->toArray(), $this->getUserConfig()->getCreateUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new LicenseClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new LicenseClientException('Syntax error, malformed JSON', json_last_error());
        }

        return UsersCreateResponseFactory::create($content);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws LicenseClientException|Exception
     */
    public function update(UserCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendPutRequest($request->toArray(), $this->getUserConfig()->getUpdateUrl());
        if ($response->code !== HttpStatusCodeType::NO_CONTENT) {
            throw new LicenseClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @param UserDeleteRequest $request
     *
     * @return NoContentResponse
     *
     * @throws LicenseClientException|Exception
     */
    public function delete(UserDeleteRequest $request)
    {
        if ($request->validate() === false) {
            throw new LicenseClientException($request->getStringErrors());
        }

        $response = $this->sendDeleteRequest($request->toArray(), $this->getUserConfig()->getDeleteUrl());
        if ($response->code !== HttpStatusCodeType::ACCEPTED) {
            throw new LicenseClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @return UserConfig
     */
    private function getUserConfig()
    {
        return $this->userConfig;
    }
}
