<?php
namespace LicenseServerClient\Services;

use LicenseServerClient\Services\Api\LicenseApiServiceInterface;
use LicenseServerClient\Services\Api\UserApiServiceInterface;

class LicenseClientService
{

    /**
     * @var LicenseApiServiceInterface
     */
    private $licenseApiService;

    /**
     * @var UserApiServiceInterface
     */
    private $userApiService;

    /**
     * @param LicenseApiServiceInterface $licenseApiService
     * @param UserApiServiceInterface $userApiService
     */
    function __construct(
        LicenseApiServiceInterface $licenseApiService,
        UserApiServiceInterface    $userApiService
    )
    {
        $this->licenseApiService = $licenseApiService;
        $this->userApiService = $userApiService;
    }

    /**
     * @return LicenseApiServiceInterface
     */
    public function getLicenseApiService()
    {
        return $this->licenseApiService;
    }

    /**
     * @return UserApiServiceInterface
     */
    public function getUserApiService()
    {
        return $this->userApiService;
    }
}
