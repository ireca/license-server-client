<?php
namespace LicenseServerClient\Factories\Services;

use Doctrine\ORM\ORMException;
use LicenseServerClient\Components\Configuration;
use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Services\Api\LicenseApiService;
use LicenseServerClient\Services\Api\UserApiService;
use LicenseServerClient\Services\ConfigService;
use LicenseServerClient\Services\Factories\HttpRequestServiceFactory;
use LicenseServerClient\Services\LicenseClientService;
use LicenseServerClient\Services\RequestServiceInterface;
use LicenseServerClient\Types\ProtocolType;
use RestLog\Factory\MessageLogFactory;
use SlackErrorNotifier\Factory\ErrorHandlerFactory;

class LicenseClientFactory
{

    /**
     * @param array $config
     * @param int $companyId
     * @param int $sourceId
     *
     * @return LicenseClientService
     *
     * @throws ORMException
     * @throws LicenseClientException
     */
    public static function create($config, $companyId = null, $sourceId = null)
    {
        $configService = self::getConfigService($config);

        $requestService = self::getRequestService($configService);
        $logger = MessageLogFactory::createByConfig($configService->getRestLogConfig())->init($companyId, null, $sourceId);
        $notifier = ErrorHandlerFactory::createByConfig($configService->getSlackNotifier());

        return new LicenseClientService(
            new LicenseApiService($configService->getLicenseConfig(), $requestService, $logger, $notifier),
            new UserApiService($configService->getUserConfig(), $requestService, $logger, $notifier)
        );
    }

    /**
     * @param array $config
     *
     * @return ConfigService
     *
     * @throws LicenseClientException
     */
    private static function getConfigService(array $config)
    {
        $configService = new ConfigService(new Configuration(), $config);
        if (! $configService->isValidate()) {
            throw new LicenseClientException($configService->getErrorMessage());
        }

        return $configService;
    }

    /**
     * @param $configService
     *
     * @return RequestServiceInterface
     *
     * @throws LicenseClientException
     */
    private static function getRequestService($configService)
    {
        switch ($configService->getRequestClient()) {
            case ProtocolType::HTTP_REQUEST_SERVICE :
                $requestService = HttpRequestServiceFactory::create($configService);
                break;
            default:
                throw new LicenseClientException('Unrecognized request client, check config licenseServerClient.requestClient');
        }

        return $requestService;
    }
}
