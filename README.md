# License Server Client - license management client

License Server Client - this service allows you to manage various licenses and notify other customers about the status of their license.

## Installation

Install the latest version with

```bash
$ composer require ireca/license-server-client
# from vendor/antonioo83/rest-log
$ composer install
# config/database-config.php Set up database connections in config
$ php vendor/doctrine/migrations/bin/doctrine-migrations.php migrations:migrate --configuration config/migrations-config.yml --db-configuration config/database-config.php
# create .env file in root and set config variables 
#
# BASE_URI='' - server url
# AUTHORIZATION='' - Bearer auth
#
# DB settings for save logs
# DB_HOST='' - DB host
# DB_PORT='' - DB port
# DB_USER='' - DB user
# DB_PASSWORD='' - DB password
# DB_NAME='' - DB name
#
# Slack settings fot notifications errors
# SLACK_NOTIFIER_SERVER=''
# SLACK_NOTIFIER_HOOK_URL=''
# SLACK_NOTIFIER_CHANEL=''
$ touch .env
```

## Basic Usage

```php
<?php

require_once 'vendor/autoload.php';

use LicenseServerClient\Factories\Services\LicenseClientFactory;
use LicenseServerClient\Requests\Licenses\LicenseCreateRequest;
use LicenseServerClient\Requests\Licenses\LicenseDeleteRequest;
use LicenseServerClient\Requests\Licenses\LicenseGetRequest;
use LicenseServerClient\Requests\Licenses\LicenseRequest;
use LicenseServerClient\Requests\Users\ProductRequest;
use LicenseServerClient\Requests\Users\UserCreateRequest;
use LicenseServerClient\Requests\Users\UserDeleteRequest;
use LicenseServerClient\Requests\Users\UserGetRequest;
use LicenseServerClient\Requests\Users\UsersGetRequest;
use LicenseServerClient\Responses\Factories\CallbackResponseFactory;

$config = require('config/config.php');

$licenseClient = LicenseClientFactory::create($config);

$productRequest = new ProductRequest(
    type: '', 
    permissions: array('create', 'update', 'delete', 'get')
);
$userCreateRequest = new UserCreateRequest(
    userId: '',
    role: '',
    title: '',
    description: '',
    products: array($productRequest)
);

/** Creat user */
$data = $licenseClient->getUserApiService()->create($userCreateRequest);

$productRequest = new ProductRequest(
    type: '', 
    permissions:  array('create', 'update')
);

$userUpdateRequest = new UserCreateRequest(
    userId: '',
    role: '',
    title: '',
    description: '',
    products: array($productParams)
);

/** Update user */
$data = $licenseClient->getUserApiService()->update($userUpdateRequest);

$usersDeleteRequest = new UserDeleteRequest(userId: '');

/** Delete user */
$data = $licenseClient->getUserApiService()->delete($usersDeleteRequest);

/** Get list users */
$usersGetRequest = new UsersGetRequest(limit: 1000, offset: 0);

$data = $licenseClient->getUserApiService()->read($usersGetRequest);

/** Get user */
$userGetRequest = new UserGetRequest(userId: '');

$data = $licenseClient->getUserApiService()->read($userGetRequest);

$licenseRequest = new LicenseRequest(
    licenseId: '',
    productType: '',
    callbackUrl: '',
    count: 1,
    licenseKey: '',
    activationAt: '',
    expirationAt: '',
    description: ''
);

$licenseCreateRequest = new LicenseCreateRequest(
    customerId: '',
    type: '',
    inn: '',
    title: '',
    description: '',
    licenses: array($licenseRequest)
);

/** Create or update license */
$data = $licenseClient->getLicenseApiService()->replace($licenseCreateRequest);

$licenseDeleteRequest = new LicenseDeleteRequest(
    customerId: '',
    licenseId: ''
);

/** Delete license */
$data = $licenseClient->getLicenseApiService()->delete($licenseDeleteRequest);

$licenseGetRequest = new LicenseGetRequest(
    customerId: '',
    licenseId: ''
);

/** Get license */
$data = $licenseClient->getLicenseApiService()->read($licenseGetRequest);

$jsonValidString = '{
  "customerId": "",
  "licenseId": "",
  "productType": "",
  "callbackUrl": "",
  "count": 1,
  "licenseKey": "",
  "activationAt": "",
  "expirationAt": "",
  "description": ""
}';
$callback = CallbackResponseFactory::create($jsonValidString);


```

### Requirements

S3 Storage Service works with

- PHP 5.5 or above
- Mysql 5.7 or above

### Submitting bugs and feature requests

Bugs and feature request are tracked on [GitLab](https://gitlab.com/dashboard/issues?assignee_username=eye-of-dev)

### Tests

php vendor/bin/codecept run unit

### License

License Server Client is licensed under the MIT License - see the `LICENSE` file for details
