<?php
namespace LicenseServerClient\unit;

use Codeception\Test\Unit;
use Doctrine\ORM\ORMException;
use LicenseServerClient\Exceptions\LicenseClientException;
use LicenseServerClient\Factories\Services\LicenseClientFactory;
use LicenseServerClient\Requests\Licenses\LicenseGetRequest;
use LicenseServerClient\Requests\Licenses\LicenseDeleteRequest;
use LicenseServerClient\Requests\Licenses\LicenseCreateRequest;
use LicenseServerClient\Requests\Licenses\LicenseRequest;
use LicenseServerClient\Responses\Licenses\LicenseItemResponse;
use LicenseServerClient\Responses\Licenses\LicensesReplaceResponseList;
use LicenseServerClient\Responses\NoContentResponse;
use LicenseServerClient\Services\LicenseClientService;
use LicenseServerClient\UnitTester;

class LicensesTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var LicenseClientService
     */
    private $licenseClient;

    /**
     * @var string
     */
    private $customerId = '6371863a-cb98-4847-8d77-c9b1cb983ced';

    /**
     * @var string
     */
    private $licenseId = '54c511dd-e4e9-4bd3-bf27-e0380d8eeda2';

    /**
     * @return void
     *
     * @throws LicenseClientException
     * @throws ORMException
     */
    protected function _before()
    {
        $config = require(dirname(dirname(dirname(__DIR__))).'/config/config.php');

        $this->licenseClient = LicenseClientFactory::create($config);
    }

    protected function _after()
    {
    }

    /**
     *
     * @return void
     */
    public function testLicensesReplace()
    {
        $licenseRequest = new LicenseRequest(
            $this->getLicenseId(),
            'waiter',
            'https://my.sitename.ru/callback',
            3,
            'c243a9d2-37f1-4c5c-8653-b3272c095def',
            '2023-04-05 00:00:00',
            '2023-04-10 00:00:00',
            'The license of the waiter application'
        );

        $licensesCreateRequest = new LicenseCreateRequest(
            $this->getCustomerId(),
            'service',
            '1234567890',
            'Test Licenses',
            array($licenseRequest)
        );

        $data = $this->licenseClient->getLicenseApiService()->replace($licensesCreateRequest);

        $this->assertInstanceOf(LicensesReplaceResponseList::class, $data);
    }

    /**
     * @depends testLicensesReplace
     *
     * @return void
     */
    public function testLicensesItem()
    {
        $licenseGetRequest = new LicenseGetRequest(
            $this->getCustomerId(),
            $this->getLicenseId()
        );

        $data = $this->licenseClient->getLicenseApiService()->getLicense($licenseGetRequest);

        $this->assertInstanceOf(LicenseItemResponse::class, $data);
    }

    /**
     * @depends testLicensesItem
     *
     * @return void
     */
    public function testLicensesDelete()
    {
        $licensesDeleteRequest = new LicenseDeleteRequest(
            '6371863a-cb98-4847-8d77-c9b1cb983ced',
            '54c511dd-e4e9-4bd3-bf27-e0380d8eeda2'
        );

        $data = $this->licenseClient->getLicenseApiService()->delete($licensesDeleteRequest);

        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @return string
     */
    private function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    private function getLicenseId()
    {
        return $this->licenseId;
    }
}
