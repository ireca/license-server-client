<?php
namespace LicenseServerClient\unit;

use Codeception\Test\Unit;
use Doctrine\ORM\ORMException;
use LicenseServerClient\Exceptions;
use LicenseServerClient\Factories\Services\LicenseClientFactory;
use LicenseServerClient\Requests\Users\ProductRequest;
use LicenseServerClient\Requests\Users\UserDeleteRequest;
use LicenseServerClient\Requests\Users\UserGetRequest;
use LicenseServerClient\Requests\Users\UserCreateRequest;
use LicenseServerClient\Requests\Users\UsersGetRequest;
use LicenseServerClient\Responses\NoContentResponse;
use LicenseServerClient\Responses\Users\UserReadResponse;
use LicenseServerClient\Responses\Users\UsersCreateResponse;
use LicenseServerClient\Responses\Users\UsersReadResponseList;
use LicenseServerClient\Services\LicenseClientService;
use LicenseServerClient\Types\PermissionType;
use LicenseServerClient\UnitTester;

class UsersTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var LicenseClientService
     */
    private $licenseClient;

    /**
     * @var string
     */
    private $userId = '909fdfea-fff1-4dd5-97f7-612cf9840b-8';

    /**
     * @return void
     *
     * @throws Exceptions\LicenseClientException
     * @throws ORMException
     */
    protected function _before()
    {
        $config = require(dirname(dirname(dirname(__DIR__))).'/config/config.php');

        $this->licenseClient = LicenseClientFactory::create($config);
    }

    protected function _after()
    {
    }

    /**
     * @return void
     */
    public function testCreate()
    {
        $productParams = new ProductRequest('courier', array(PermissionType::CREATE, PermissionType::UPDATE, PermissionType::DELETE, PermissionType::GET));
        $userPostParams = new UserCreateRequest(
            $this->getUserId(),
            'service',
            'Test service users',
            'Test user to be deleted after tests',
            array($productParams)
        );

        $data = $this->licenseClient->getUserApiService()->create($userPostParams);

        $this->assertInstanceOf(UsersCreateResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     */
    public function testUpdate()
    {
        $productRequest = new ProductRequest('courier', array(PermissionType::CREATE, PermissionType::UPDATE));
        $userPutRequest = new UserCreateRequest(
            $this->getUserId(),
            'service',
            'Test service users',
            'Test user to be deleted after tests',
            array($productRequest)
        );
        $data = $this->licenseClient->getUserApiService()->update($userPutRequest);

        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @depends testUpdate
     *
     * @return void
     */
    public function testGetUser()
    {
        $userGetRequest = new UserGetRequest($this->getUserId());
        $data = $this->licenseClient->getUserApiService()->getUser($userGetRequest);

        $this->assertInstanceOf(UserReadResponse::class, $data);
    }

    /**
     * @depends testGetUser
     *
     * @return void
     */
    public function testGetUsersList()
    {
        $usersGetRequest = new UsersGetRequest(1000, 0);
        $data = $this->licenseClient->getUserApiService()->getUsers($usersGetRequest);

        $this->assertInstanceOf(UsersReadResponseList::class, $data);
    }

    /**
     * @depends testGetUsersList
     *
     * @return void
     */
    public function testDelete()
    {
        $usersDeleteRequest = new UserDeleteRequest($this->getUserId());
        $data = $this->licenseClient->getUserApiService()->delete($usersDeleteRequest);

        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @return string
     */
    private function getUserId()
    {
        return $this->userId;
    }
}
